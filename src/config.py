import os

kafka_brokers = os.getenv('KAFKA_BROKERS', 'localhost:29792')
kafka_topic_users_updates = 'user-updates'
